import React from "react";
import { PokemonPicker } from "./pokemons/PokemonPicker";

export const Main = () => {
  return (
    <div>
      <PokemonPicker />
    </div>
  );
};
