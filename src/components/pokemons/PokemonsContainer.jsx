import React, { useState, useEffect } from "react";
import { Pokemon } from "./Pokemon";
import { getPokemonByName } from "./PokemonApi";
import Grid from "@material-ui/core/Grid";

export const PokemonsContainer = ({ choosedPokemon }) => {
  const [myPokemon, setMyPokemon] = useState("");
  const [enemyPokemon, setEnemyPokemon] = useState("");
  const [loading, setLoading] = useState(true);
  const [choosedEnemyPokemon, setChoosedEnemyPokemon] = useState("pikachu");

  async function getMyPokemon() {
    const { data } = await getPokemonByName(choosedPokemon);
    setMyPokemon(data);
  }
  async function getEnemyPokemon() {
    const { data } = await getPokemonByName(choosedEnemyPokemon);
    setEnemyPokemon(data);
    setLoading(false);
  }

  useEffect(() => {
    getMyPokemon();
    getEnemyPokemon();
  }, []);

  if (loading) {
    return <p>Loading...</p>;
  }

  return (
    console.log("myPokemon", myPokemon) &&
    console.log("ennemy", enemyPokemon) && (
      <div>
        <Grid
          container
          spacing={3}
          direction="row"
          alignItems="center"
          justify="center"
          style={{ minHeight: "100vh" }}
        >
          <Pokemon myPokemon={myPokemon} title={"My Pokemon"} />
          <Grid item>VS</Grid>
          <Pokemon
            myPokemon={enemyPokemon}
            title={"The Pokemon she tells me not to worry about"}
          />
        </Grid>
      </div>
    )
  );
};
