const rootUrl = "https://pokeapi.co/api/v2/pokemon/";

export async function getPokemonByName(name) {
  const response = await fetch(`${rootUrl}${name}`);
  const json = await response.json();
  return { data: json };
}

export async function getAllPokemons() {
  const response = await fetch(`${rootUrl}?limit=964`);
  const json = await response.json();
  return { data: json };
}
