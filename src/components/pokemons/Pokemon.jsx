import React from "react";
import { PokemonCard } from "./PokemonCard";

export const Pokemon = ({ myPokemon, title }) => {
  return <PokemonCard myPokemon={myPokemon} title={title} />;
};
