import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import firstLetterUppercase from "../../utils/firstLetterUppercase";

const useStyles = makeStyles({
  card: {
    maxWidth: 345
  },
  media: {
    height: 140
  }
});

export const PokemonCard = ({ myPokemon, title, ...props }) => {
  console.log(props);
  const classes = useStyles();
  const { name, sprites, types, height, weight } = myPokemon;
  return (
    <Grid item>
      {/* <p>{title}</p> */}
      <Card className={classes.card}>
        <CardActionArea>
          <CardMedia
            className={classes.media}
            image={sprites.front_default}
            title={`${firstLetterUppercase(name)}'s default front sprite`}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {firstLetterUppercase(name)}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              <p>
                Type(s) :{" "}
                {types.map(t => firstLetterUppercase(t.type.name) + "  ")}
              </p>
              Height / Weight : {height} | {weight}
            </Typography>
          </CardContent>
        </CardActionArea>
        {/* <CardActions>
            <Button size="small" color="primary">
              Share
            </Button>
            <Button size="small" color="primary">
              Learn More
            </Button>
          </CardActions> */}
      </Card>
    </Grid>
  );
};
