import React, { useState, useEffect } from "react";
import { PokemonsContainer } from "./PokemonsContainer";
import { getAllPokemons } from "./PokemonApi";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import firstLetterUppercase from "../../utils/firstLetterUppercase";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));

export const PokemonPicker = () => {
  const classes = useStyles();
  const [choosedPokemon, setChoosedPokemon] = useState("");
  const [pokemons, setPokemons] = useState([]);
  const [loading, setLoading] = useState(true);

  async function getPokemons() {
    const { data } = await getAllPokemons();
    setPokemons(data.results);
    setLoading(false);
  }

  const handleChange = event => {
    setChoosedPokemon(event.target.value);
  };

  function displaySelectItems() {
    return pokemons.map(p => (
      <MenuItem value={p.name}>{firstLetterUppercase(p.name)}</MenuItem>
    ));
  }

  useEffect(() => {
    getPokemons();
  }, []);

  if (loading) {
    return "Loading...";
  }
  if (!choosedPokemon) {
    return (
      <>
        <Grid
          container
          spacing={3}
          direction="row"
          alignItems="center"
          justify="center"
          style={{ minHeight: "50vh" }}
        >
          <Grid item>
            <FormControl className={classes.formControl}>
              <InputLabel id="demo-simple-select-label">Pokémons</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={pokemons}
                onChange={handleChange}
              >
                {displaySelectItems()}
              </Select>
            </FormControl>
          </Grid>
        </Grid>
      </>
    );
  } else {
    return <PokemonsContainer choosedPokemon={choosedPokemon} />;
  }
};
